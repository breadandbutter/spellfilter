/*
 * Copyright (C) 2012 breadandbutter <breadandbutter@tormail.org>
 *
 * This file is part of SpellFilter.
 * 
 * SpellFilter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SpellFilter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SpellFilter.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "spellfilter.h"
#include "spellfilter_operation.h"

void SpellFilter::FilterSpellFamilyName(uint32_t name, SpellFilterOperator op)
{
    SpellEntryMap newFoundMap;
    const SpellEntryMap* usedMap;

    if (foundMap_.empty())
        usedMap = fullMap_.get();
    else
        usedMap = &foundMap_;

    for (auto itr = usedMap->begin(); itr != usedMap->end(); ++itr)
    {
        if (SpellFilterOperation(op, itr->second->SpellFamilyName, name))
            newFoundMap.insert(*itr);
    }

    foundMap_ = newFoundMap;
}

void SpellFilter::FilterSpellFamilyFlags(uint64_t flags, SpellFilterOperator op)
{
    SpellEntryMap newFoundMap;
    const SpellEntryMap* usedMap;

    if (foundMap_.empty())
        usedMap = fullMap_.get();
    else
        usedMap = &foundMap_;

    for (auto itr = usedMap->begin(); itr != usedMap->end(); ++itr)
    {
        if (SpellFilterOperation(op, itr->second->SpellFamilyFlags, flags))
            newFoundMap.insert(*itr);
    }

    foundMap_ = newFoundMap;
}
