/*
 * Copyright (C) 2012 breadandbutter <breadandbutter@tormail.org>
 *
 * This file is part of SpellFilter.
 * 
 * SpellFilter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SpellFilter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SpellFilter.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include <cassert>
#include "spellfilter.h"

SpellFilter::SpellFilter(SpellEntryMapPtr spellMap)
{
    assert(spellMap != nullptr);
    fullMap_ = spellMap;
}

SpellFilter::~SpellFilter()
{
    
}

void SpellFilter::Print() const
{
    if (foundMap_.empty())
    {
        std::cerr << "No spells found" << std::endl;
        return;
    }

    for (auto itr = foundMap_.begin(); itr != foundMap_.end(); ++itr)
    {
        auto spell = (*itr).second;

        // Name, rank and Id
        std::cout << spell->SpellName[0] << " ";
        if (std::string(spell->Rank[0]).length())
            std::cout << "(" << spell->Rank[0] << ")";
        std::cout << " [" << spell->Id << "]" << std::endl;
    }
}
