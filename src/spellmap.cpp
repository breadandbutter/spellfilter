/*
 * Copyright (C) 2012 breadandbutter <breadandbutter@tormail.org>
 *
 * This file is part of SpellFilter.
 * 
 * SpellFilter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SpellFilter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SpellFilter.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "spellmap.h"
#include "dbcreader/DBCFileLoader.h"


// TODO: This is totally fucked up, unfuck it
SpellEntryMapPtr CreateSpellMap(const char *dbfile)
{
    const char *dbformat = "nixiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiifxiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiffffffiiiiiiiiiiiiiiiiiiiiifffiiiiiiiiiiiiiiifffixiixssssssssssssssssxssssssssssssssssxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxiiiiiiiiiixfffxxxiiii";

    if (dbfile == nullptr)
        dbfile = "dbc/Spell.dbc";

    DBCFileLoader dbc;
    if (!dbc.Load(dbfile, dbformat))
        throw std::runtime_error("Failed to load DBC file");

    SpellEntry **indexTable;
    SpellEntry *dataTable;

    uint32_t a = 0;
    dataTable = (SpellEntry *)dbc.AutoProduceData(dbformat, a, (char **&)indexTable);
    char *strings = dbc.AutoProduceStrings(dbformat, (char *)dataTable);
    
    SpellEntryMap *spellMap = new SpellEntryMap;
    for (size_t i = 0; i < dbc.GetNumRows(); i++)
        (*spellMap)[dataTable[i].Id] = &dataTable[i];
    
    return SpellEntryMapPtr(spellMap);
}
