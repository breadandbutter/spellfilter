/*
 * Copyright (C) 2012 breadandbutter <breadandbutter@tormail.org>
 *
 * This file is part of SpellFilter.
 * 
 * SpellFilter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SpellFilter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SpellFilter.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "spellfilter.h"
#include "spellmap.h"

int main(int argc, char **argv)
{
    try
    {
        SpellEntryMapPtr spellMap = CreateSpellMap();
        SpellFilter filter(spellMap);
        filter.FilterSpellFamilyName(8);
        filter.FilterSpellFamilyFlags(256);
        filter.Print();
    }
    catch (std::runtime_error const& e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }
}
