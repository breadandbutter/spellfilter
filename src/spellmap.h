/*
 * Copyright (C) 2012 breadandbutter <breadandbutter@tormail.org>
 *
 * This file is part of SpellFilter.
 * 
 * SpellFilter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SpellFilter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SpellFilter.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef __SPELLFILTER_SPELLMAP_H
#define __SPELLFILTER_SPELLMAP_H
#include <map>
#include <memory>
#include <cstdint>
#include <stdexcept>

#include "dbcreader/SpellEntry.h"

typedef std::map<uint32_t, SpellEntry const*> SpellEntryMap;
typedef std::shared_ptr<const SpellEntryMap> SpellEntryMapPtr;

SpellEntryMapPtr CreateSpellMap(const char *dbfile = nullptr);

#endif
