/*
 * Copyright (C) 2012 breadandbutter <breadandbutter@tormail.org>
 *
 * This file is part of SpellFilter.
 * 
 * SpellFilter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SpellFilter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SpellFilter.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef __SPELLFILTER_OPERATION_H
#define __SPELLFILTER_OPERATION_H

enum class SpellFilterOperator
{
    EQUAL_TO,
    DIFFERENT_FROM,
    GREATER_THAN,
    GREATER_THAN_OR_EQUAL_TO,
    LESS_THAN,
    LESS_THAN_OR_EQUAL_TO,
    MASK_EXACT,
    MASK_ANY,
};

template <typename T>
inline bool SpellFilterOperation(SpellFilterOperator op, T a, T b)
{
    switch (op)
    {
        case SpellFilterOperator::EQUAL_TO:
            return a == b;
        case SpellFilterOperator::DIFFERENT_FROM:
            return a != b;
        case SpellFilterOperator::GREATER_THAN:
            return a > b;
        case SpellFilterOperator::GREATER_THAN_OR_EQUAL_TO:
            return a >= b;
        case SpellFilterOperator::LESS_THAN:
            return a < b;
        case SpellFilterOperator::LESS_THAN_OR_EQUAL_TO:
            return a <= b;
        case SpellFilterOperator::MASK_EXACT:
            return (a & b) == b;
        case SpellFilterOperator::MASK_ANY:
            return a & b;
    }
}

#endif
