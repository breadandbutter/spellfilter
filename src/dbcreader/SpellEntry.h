/*
 * Copyright (C) 2005-2012 MaNGOS <http://getmangos.com/>
 * Copyright (C) 2012 breadandbutter <breadandbutter@tormail.org>
 *
 * This file is part of SpellFilter.
 * 
 * SpellFilter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SpellFilter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SpellFilter.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef __SPELLENTRY_H_
#define __SPELLENTRY_H_

#define MAX_EFFECT_INDEX 3
#define MAX_SPELL_REAGENTS 8
#define MAX_SPELL_TOTEMS 2
#define MAX_SPELL_TOTEM_CATEGORIES 2

struct SpellEntry
{
    uint32_t    Id;                                           // 0        m_ID
    uint32_t    Category;                                     // 1        m_category
    //uint32_t     castUI                                     // 2 not used
    uint32_t    Dispel;                                       // 3        m_dispelType
    uint32_t    Mechanic;                                     // 4        m_mechanic
    uint32_t    Attributes;                                   // 5        m_attributes
    uint32_t    AttributesEx;                                 // 6        m_attributesEx
    uint32_t    AttributesEx2;                                // 7        m_attributesExB
    uint32_t    AttributesEx3;                                // 8        m_attributesExC
    uint32_t    AttributesEx4;                                // 9        m_attributesExD
    uint32_t    AttributesEx5;                                // 10       m_attributesExE
    uint32_t    AttributesEx6;                                // 11       m_attributesExF
    uint32_t    Stances;                                      // 12       m_shapeshiftMask
    uint32_t    StancesNot;                                   // 13       m_shapeshiftExclude
    uint32_t    Targets;                                      // 14       m_targets
    uint32_t    TargetCreatureType;                           // 15       m_targetCreatureType
    uint32_t    RequiresSpellFocus;                           // 16       m_requiresSpellFocus
    uint32_t    FacingCasterFlags;                            // 17       m_facingCasterFlags
    uint32_t    CasterAuraState;                              // 18       m_casterAuraState
    uint32_t    TargetAuraState;                              // 19       m_targetAuraState
    uint32_t    CasterAuraStateNot;                           // 20       m_excludeCasterAuraState
    uint32_t    TargetAuraStateNot;                           // 21       m_excludeTargetAuraState
    uint32_t    CastingTimeIndex;                             // 22       m_castingTimeIndex
    uint32_t    RecoveryTime;                                 // 23       m_recoveryTime
    uint32_t    CategoryRecoveryTime;                         // 24       m_categoryRecoveryTime
    uint32_t    InterruptFlags;                               // 25       m_interruptFlags
    uint32_t    AuraInterruptFlags;                           // 26       m_auraInterruptFlags
    uint32_t    ChannelInterruptFlags;                        // 27       m_channelInterruptFlags
    uint32_t    procFlags;                                    // 28       m_procTypeMask
    uint32_t    procChance;                                   // 29       m_procChance
    uint32_t    procCharges;                                  // 30       m_procCharges
    uint32_t    maxLevel;                                     // 31       m_maxLevel
    uint32_t    baseLevel;                                    // 32       m_baseLevel
    uint32_t    spellLevel;                                   // 33       m_spellLevel
    uint32_t    DurationIndex;                                // 34       m_durationIndex
    uint32_t    powerType;                                    // 35       m_powerType
    uint32_t    manaCost;                                     // 36       m_manaCost
    uint32_t    manaCostPerlevel;                             // 37       m_manaCostPerLevel
    uint32_t    manaPerSecond;                                // 38       m_manaPerSecond
    uint32_t    manaPerSecondPerLevel;                        // 39       m_manaPerSecondPerLevel
    uint32_t    rangeIndex;                                   // 40       m_rangeIndex
    float     speed;                                        // 41       m_speed
    //uint32_t    modalNextSpell;                             // 42       m_modalNextSpell not used
    uint32_t    StackAmount;                                  // 43       m_cumulativeAura
    uint32_t    Totem[MAX_SPELL_TOTEMS];                      // 44-45    m_totem
    int32_t     Reagent[MAX_SPELL_REAGENTS];                  // 46-53    m_reagent
    uint32_t    ReagentCount[MAX_SPELL_REAGENTS];             // 54-61    m_reagentCount
    int32_t     EquippedItemClass;                            // 62       m_equippedItemClass (value)
    int32_t     EquippedItemSubClassMask;                     // 63       m_equippedItemSubclass (mask)
    int32_t     EquippedItemInventoryTypeMask;                // 64       m_equippedItemInvTypes (mask)
    uint32_t    Effect[MAX_EFFECT_INDEX];                     // 65-67    m_effect
    int32_t     EffectDieSides[MAX_EFFECT_INDEX];             // 68-70    m_effectDieSides
    uint32_t    EffectBaseDice[MAX_EFFECT_INDEX];             // 71-73
    float     EffectDicePerLevel[MAX_EFFECT_INDEX];         // 74-76
    float     EffectRealPointsPerLevel[MAX_EFFECT_INDEX];   // 77-79    m_effectRealPointsPerLevel
    int32_t     EffectBasePoints[MAX_EFFECT_INDEX];           // 80-82    m_effectBasePoints (don't must be used in spell/auras explicitly, must be used cached Spell::m_currentBasePoints)
    uint32_t    EffectMechanic[MAX_EFFECT_INDEX];             // 83-85    m_effectMechanic
    uint32_t    EffectImplicitTargetA[MAX_EFFECT_INDEX];      // 86-88    m_implicitTargetA
    uint32_t    EffectImplicitTargetB[MAX_EFFECT_INDEX];      // 89-91    m_implicitTargetB
    uint32_t    EffectRadiusIndex[MAX_EFFECT_INDEX];          // 92-94    m_effectRadiusIndex - spellradius.dbc
    uint32_t    EffectApplyAuraName[MAX_EFFECT_INDEX];        // 95-97    m_effectAura
    uint32_t    EffectAmplitude[MAX_EFFECT_INDEX];            // 98-100   m_effectAuraPeriod
    float     EffectMultipleValue[MAX_EFFECT_INDEX];        // 101-103  m_effectAmplitude
    uint32_t    EffectChainTarget[MAX_EFFECT_INDEX];          // 104-106  m_effectChainTargets
    uint32_t    EffectItemType[MAX_EFFECT_INDEX];             // 107-109  m_effectItemType
    int32_t     EffectMiscValue[MAX_EFFECT_INDEX];            // 110-112  m_effectMiscValue
    int32_t     EffectMiscValueB[MAX_EFFECT_INDEX];           // 113-115  m_effectMiscValueB
    uint32_t    EffectTriggerSpell[MAX_EFFECT_INDEX];         // 116-118  m_effectTriggerSpell
    float     EffectPointsPerComboPoint[MAX_EFFECT_INDEX];  // 119-121  m_effectPointsPerCombo
    uint32_t    SpellVisual;                                  // 122      m_spellVisualID
    //uint32_t    SpellVisual2;                               // 123 not used
    uint32_t    SpellIconID;                                  // 124      m_spellIconID
    uint32_t    activeIconID;                                 // 125      m_activeIconID
    //uint32_t    spellPriority;                              // 126      m_spellPriority not used
    char*     SpellName[16];                                // 127-142  m_name_lang
    //uint32_t    SpellNameFlag;                              // 143      m_name_flag not used
    char*     Rank[16];                                     // 144-159  m_nameSubtext_lang
    //uint32_t    RankFlags;                                  // 160      m_nameSubtext_flag not used
    //char*     Description[16];                            // 161-176  m_description_lang not used
    //uint32_t    DescriptionFlags;                           // 177      m_description_flag not used
    //char*     ToolTip[16];                                // 178-193  m_auraDescription_lang not used
    //uint32_t    ToolTipFlags;                               // 194      m_auraDescription_flag not used
    uint32_t    ManaCostPercentage;                           // 195      m_manaCostPct
    uint32_t    StartRecoveryCategory;                        // 196      m_startRecoveryCategory
    uint32_t    StartRecoveryTime;                            // 197      m_startRecoveryTime
    uint32_t    MaxTargetLevel;                               // 198      m_maxTargetLevel
    uint32_t    SpellFamilyName;                              // 199      m_spellClassSet
    uint64_t    SpellFamilyFlags;                             // 200-201  m_spellClassMask
    uint32_t    MaxAffectedTargets;                           // 202      m_maxTargets
    uint32_t    DmgClass;                                     // 203      m_defenseType
    uint32_t    PreventionType;                               // 204      m_preventionType
    //uint32_t    StanceBarOrder;                             // 205      m_stanceBarOrder not used
    float     DmgMultiplier[MAX_EFFECT_INDEX];              // 206-208  m_effectChainAmplitude
    //uint32_t    MinFactionId;                               // 209      m_minFactionID not used
    //uint32_t    MinReputation;                              // 210      m_minReputation not used
    //uint32_t    RequiredAuraVision;                         // 211      m_requiredAuraVision not used
    uint32_t    TotemCategory[MAX_SPELL_TOTEM_CATEGORIES];    // 212-213  m_requiredTotemCategoryID
    uint32_t    AreaId;                                       // 214
    uint32_t    SchoolMask;                                   // 215      m_schoolMask
} __attribute__((packed));

#endif
