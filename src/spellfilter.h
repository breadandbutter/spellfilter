/*
 * Copyright (C) 2012 breadandbutter <breadandbutter@tormail.org>
 *
 * This file is part of SpellFilter.
 * 
 * SpellFilter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SpellFilter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SpellFilter.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef __SPELLFILTER_SPELLFILTER_H
#define __SPELLFILTER_SPELLFILTER_H

#include <iostream>
#include "spellmap.h"
#include "spellfilter_operation.h"

class SpellFilter
{
    public:
        SpellFilter(SpellEntryMapPtr spellMap);
        ~SpellFilter();
        //TODO copy constructor
        
        // filter_spellfamily.cpp
        void FilterSpellFamilyName(uint32_t, SpellFilterOperator = SpellFilterOperator::EQUAL_TO);
        void FilterSpellFamilyFlags(uint64_t, SpellFilterOperator = SpellFilterOperator::MASK_ANY);
        
        void Print() const;

    private:
        SpellEntryMapPtr    fullMap_;
        SpellEntryMap       foundMap_;
};

#endif

